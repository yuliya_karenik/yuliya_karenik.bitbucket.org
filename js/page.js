var delta = 130;
var contentReload = false;
var callbackRegistry;

var queryPerformed;

function Page() { }

var createItem = function(itemName, content, parentElement, classList) {
	var newItem = document.createElement(itemName);
	newItem.innerHTML = content;
	for(var i = 0; i < classList.length; ++i) {
		newItem.classList.add(classList[i]);
	}
	parentElement.appendChild(newItem);
	return newItem;
}

Page.prototype.getCurrentPage = function() {
	return Math.floor(this.storage.position / this.storage.windowWidth + 1);
}

Page.prototype.goToPage = function(number) {
	var currentPage = this.getCurrentPage();
	this.paging.childNodes[currentPage - 1].classList.remove('active-item');
	this.paging.childNodes[number - 1].classList.add('active-item');
	this.storage.position = (number - 1) * this.storage.windowWidth;
	this.reload();
}

Page.prototype.reload = function() {
	this.videos.innerHTML = '';
	var currentPage = this.getCurrentPage();
	this.paging.childNodes[currentPage - 1].classList.add('active-item');
	this.storage.position = (currentPage - 1) * this.storage.windowWidth;
	var newContent = this.storage.current();
	for(var i = 0; i < newContent.length; ++i) {
		createItem('div', newContent[i], this.videos, ['video-item', 'new-content']);
	}
}

Page.prototype.makePaging = function() {
	var pagingItemsIsNecessary = Math.ceil(this.storage.content.length / this.storage.windowWidth);
	var pagingItems = this.paging.childNodes;
	while(pagingItems.length > pagingItemsIsNecessary) {
		this.paging.removeChild(this.paging.lastChild);
	}
	while(pagingItems.length < pagingItemsIsNecessary) {
		var newItem = createItem('div', '', this.paging, ['paging-item']);
		newItem.setAttribute('value', pagingItems.length);
		newItem.addEventListener('click', this.onPagingClick.bind(this));
	}
}

Page.prototype.isOk = function() {
	return this.storage.position + this.storage.windowWidth > this.storage.content.length;
}

Page.prototype.resize = function() {
	if(window.innerWidth <= 900) {
		if(this.videos.childNodes.length > 1) {
			contentReload = true;
			this.paging.childNodes[this.getCurrentPage() - 1].classList.remove('active-item');
			this.storage.windowWidth = 1;
		}
		delta = 230;
	}

	if(window.innerWidth <= 1040 && window.innerWidth > 900) {
		if(this.storage.windowWidth !== 2 && (this.videos.childNodes.length !== 2 && !this.isOk())) {
			contentReload = true;
			this.paging.childNodes[this.getCurrentPage() - 1].classList.remove('active-item');
			this.storage.windowWidth = 2;
		}
		delta = 230;
	}

	if(window.innerWidth > 1040) {
		if(this.videos.childNodes.length < 3 && queryPerformed) {
			contentReload = true;
			this.paging.childNodes[this.getCurrentPage() - 1].classList.remove('active-item');
			this.storage.windowWidth = 3;
		}
		delta = 130;
	}
	if(window.innerHeight <= 550 && window.innerWidth <= 700) {
		delta = 115;
	}
	
	this.content.style.height = (window.innerHeight - delta) + 'px';
	this.content.style.width = window.innerWidth + 'px';
	if(contentReload) {
		this.makePaging();
		this.reload();
		contentReload = false;
	}
}

Page.prototype.onKeyUp = function(e) {
	if(e.keyCode === 13) {
		queryPerformed = true;
		callbackRegistry.performQuery(true, callbackRegistry.createQueryUrl(this.textField.value), callbackRegistry.addStatistics);
	}
}

Page.prototype.transitionEnd = function(e) {
	this.videos.removeChild(e.target);
	var currentContentLength = this.storage.current().length;
	if(this.videos.childNodes.length === 0) {
		for(var i = this.storage.position; i < this.storage.position + currentContentLength; ++i) {
			var newItem = document.createElement('div');
			newItem.style.visibility = 'hidden';
			newItem.classList.add('new-content');
			newItem.innerHTML = this.storage.content[i];
			newItem.classList.add('video-item');
			this.videos.appendChild(newItem);
			newItem.style.visibility = 'visible';
		}
	}
}

Page.prototype.onPagingClick = function(e) {
	this.goToPage(e.target.getAttribute('value'));
}

var mouseIsDown = false;
var x = 0;

Page.prototype.mouseDown = function(e) {
	mouseIsDown = true;
	if(e.type === 'touchstart') {
		x = e.changedTouches[0].pageX;
	}
	else {
		x = e.clientX;
	}
}

Page.prototype.mouseUp = function(e) {
	if(mouseIsDown) {
		var direction;
		var x0 = e.type === 'touchend' ? e.changedTouches[0].pageX : e.clientX;
		if(x - x0 > 40) {
			direction = 'left';
		}
		if(x0 - x > 40) {
			direction = 'right';
		}
		if(direction !== undefined) {
			this.swipe(direction);
		}
		mouseIsDown = false;
		x = 0;
	}
}

Page.prototype.mouseOut = function() {
	mouseIsDown = false;
}

Page.prototype.animationEnd = function(e) {
	e.target.classList.remove('new-content');
}

Page.prototype.animateSwipe = function(content, translationValue) {
	for(var i = 0; i < content.length; ++i) {
		content[i].classList.add('swipe-away');
		content[i].style.transform = 'translate3d(' + translationValue + 'px, 0, 0)';
	}
}

Page.prototype.swipe = function(direction) {
	var translationValue;
	var pagingItems = this.paging.childNodes;
	var currentPage = this.getCurrentPage();
	if(direction === 'left') {
		if(this.storage.position >= this.storage.content.length - this.storage.windowWidth) {
			callbackRegistry.performQuery(false, callbackRegistry.createQueryUrl(this.textField.value), callbackRegistry.addStatistics);
			return;
		}
		this.storage.forward();
		pagingItems[currentPage - 1].classList.remove('active-item');
		pagingItems[currentPage].classList.add('active-item');
		translationValue = -window.innerWidth;
	}
	else {
		if(this.storage.position === 0) {
			return;
		}
		this.storage.backward();
		pagingItems[currentPage - 1].classList.remove('active-item');
		pagingItems[currentPage - 2].classList.add('active-item');
		translationValue = window.innerWidth;
	}
	this.animateSwipe(this.videos.childNodes, translationValue);
}

Page.prototype.beforeunload = function() {
	window['query'] = this.textField.value;
}

var init = function() {
	document.body.innerHTML = '<header><input type="text" autocomplete="off" placeholder="example: javascript">\
						<input type="button" value="search">\
						</header>\
						<div class="content">\
						<div id="videos"></div>\
						</div>\
						<footer>\
						<ul id="paging"></ul>\
						</footer>';
	this.paging = document.getElementById('paging');
	this.videos = document.getElementById('videos');
	var innerWidth = window.innerWidth;
	var windowWidth = innerWidth >= 1040 ? 3 : (innerWidth < 1040 && innerWidth >= 900) ? 2 : 1;
	delta = innerWidth >= 1040 ? 130 : (innerWidth < 1040 && innerWidth >= 900) ? 230 :
	(innerWidth <= 700 && window.innerHeight < 550) ? 115 : 130;
	this.storage = new Iterator({windowWidth: windowWidth});
	callbackRegistry = new Query(this);
	queryPerformed = false;
	this.content = document.querySelector('.content');
	this.textField = document.querySelector('input[type="text"]');
	this.textField.addEventListener('keyup', this.onKeyUp.bind(this));
	this.textField.focus();
	this.searchButton = document.querySelector('input[type="button"]');
	this.videos.addEventListener('webkitTransitionEnd', this.transitionEnd.bind(this));
	this.videos.addEventListener('webkitAnimationEnd', this.animationEnd.bind(this));
	this.videos.addEventListener('mousedown', this.mouseDown.bind(this));
	this.videos.addEventListener('touchstart', this.mouseDown.bind(this));
	this.searchButton.addEventListener('click',	this.onKeyUp.bind(this, {keyCode: 13}));
	this.videos.addEventListener('mouseup', this.mouseUp.bind(this));
	this.videos.addEventListener('touchend', this.mouseUp.bind(this));
	window.addEventListener('resize', this.resize.bind(this));
	this.content.style.height = (window.innerHeight - delta) + 'px';
	this.content.style.width = window.innerWidth + 'px';
}