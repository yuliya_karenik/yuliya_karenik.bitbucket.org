var Query = function(pageObject) {
	this.pageObject = pageObject;
	this.nextPageToken = '';
}

Query.prototype.createQueryUrl = function(query) {
	return 'https://www.googleapis.com/youtube/v3/search?part=snippet,id'
	+ (this.nextPageToken === '' ? '' : '&pageToken=' + this.nextPageToken) + '&maxResults=15&q=' + query +
	'&videoCaption=closedCaption&type=video&key=AIzaSyA1sZL8gRiCZcwcfdusRDD1uqgy9wpME3M';
}

Query.prototype.createStatisticsUrl = function(ids) {
	var url = 'https://www.googleapis.com/youtube/v3/videos?part=statistics,id&id=';
	for(var i = 0; i < ids.length; ++i) {
		url += (i === 0 ? '' : ',') + ids[i];
	}
	return url + '&key=AIzaSyA1sZL8gRiCZcwcfdusRDD1uqgy9wpME3M';
}

Query.prototype.getIds = function(data) {
	var result = [];
	for(var i = 0; i < data.items.length; ++i) {
		result.push(data.items[i].id.videoId);
	}
	return result;
}

var restructure = function(data) {
	var result = {};
	for(var i = 0; i < data.items.length; ++i) {
		result[data.items[i].id] = data.items[i].statistics.viewCount;
	}
	return result;
}

Query.prototype.addStatistics = function(firstCall, data) {
	this.response = data;

	var id = this.getIds(data);
	var url = this.createStatisticsUrl(id);

	this.performQuery(firstCall, url, this.appendContent);
};

Query.prototype.appendContent = function(firstCall, data) {

	var statistics = restructure(data);

	this.pageObject.storage.content.splice.apply(this.pageObject.storage.content,
		[this.pageObject.storage.content.length, 0].concat(this.processData(this.response, statistics)));
	this.pageObject.makePaging();
	if(firstCall) {
		this.pageObject.reload();
	}
	this.pageObject.resize();

	//this.pageObject.resize();
	//this.pageObject.paging.childNodes[this.pageObject.getCurrentPage() - 1].classList.add('active-item');
	if(!firstCall) {
		this.pageObject.swipe('left');
	}
}

Query.prototype.performQuery = function(firstCall, url, onSuccess, onError) {
	if(firstCall) {
		this.pageObject.videos.innerHTML = '';
		this.pageObject.paging.innerHTML = '';
		this.pageObject.storage.position = 0;
	}
	var scriptOK = false;
	var callbackName = 'cb' + String(Math.random()).slice(2);
	var urlWithCb = url + '&callback=callbackRegistry.' + callbackName;
	
	this[callbackName] = function(data) {
		scriptOk = true;
		delete this[callbackName];
		onSuccess.bind(this, firstCall, data)();
  	};

	function checkCallback() {
		if(scriptOk) return;
		delete this[callbackName];
		this.handleError(urlWithCb);
		//onError(url);
	}

	var script = document.createElement('script');
	script.onload = script.onerror = checkCallback;
  	script.src = urlWithCb;
  	document.body.appendChild(script);
  	//return callbackResult;
};

Query.prototype.processData = function(data, statistics) {
	var items = data.items;
	var result = [];
	this.nextPageToken = data.nextPageToken;
	for(var i = 0; i < items.length; ++i) {
		var videoId = items[i].id.videoId;
		var snippet = items[i].snippet;
		result.push(
			'<h3><a href="https://www.youtube.com/watch?v=' + videoId + '">' + snippet.title + '</a></h3>' +
			'<center><a href="https://www.youtube.com/watch?v=' + videoId +
			'"><img src="' + snippet.thumbnails.medium.url + '" alt="video preview"></a></center>' +
			'<label>Author: ' + snippet.channelTitle + '</label>' +
			'<label>Published at: ' + snippet.publishedAt.slice(0, 10) + '</label>' +
			'<label>View count: ' + statistics[videoId] + '</label>' +
			'<label>Description: ' + snippet.description + '</label>'
			)
	}
	return result;
}

Query.prototype.handleError = function(url) {
	alert('error');
}