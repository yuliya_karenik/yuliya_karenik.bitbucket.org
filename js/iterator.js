function Iterator(cfg) {
	var config = cfg || {};
	this.content = config.content || [];
	this.windowWidth = config.windowWidth === undefined ? 1 : config.windowWidth;
	//this.windowWidth = config.windowWidth === undefined ? 1 : Math.min(config.windowWidth, this.content.length);
	this.position = 0;
}

Iterator.prototype.move = function(step) {
	this.position += step;
	return this.content.slice(this.position, Math.min(this.position + this.windowWidth, this.content.length));
}

Iterator.prototype.forward = function() {
	var maxPossibleStep = Math.min(this.windowWidth, this.content.length - this.position - 1);
	return this.move(maxPossibleStep);
}

Iterator.prototype.backward = function() {
	var maxPossibleStep = Math.min(this.windowWidth, this.position);
	return this.move(-maxPossibleStep);
}

Iterator.prototype.current = function() {
	return this.content.slice(this.position, Math.min(this.position + this.windowWidth, this.content.length));
}

Iterator.prototype.jumpTo = function(position) {
	var maxPossiblePosition = this.content.length - this.windowWidth;
	this.position = position < 0 ? 0 : position > maxPossiblePosition ? maxPossiblePosition : position;
}